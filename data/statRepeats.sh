#!/bin/bash

echo "Pocetak izvrsavanja"

for subdir in */; do
    subdir_name=${subdir%/}
    
    echo "---------------- $subdir_name ----------------"
    echo "\t-dn"
    ./StatRepeats.v1.r6 "$subdir_name/$subdir_name.fasta" 12 -dn -load "$subdir_name/izlaz_dn"
    echo "\t-dc"
    ./StatRepeats.v1.r6 "$subdir_name/$subdir_name.fasta" 12 -dc -load "$subdir_name/izlaz_dc"
    echo "\t-in"
    ./StatRepeats.v1.r6 "$subdir_name/$subdir_name.fasta" 12 -in -load "$subdir_name/izlaz_in"
    echo "\t-ic"
    ./StatRepeats.v1.r6 "$subdir_name/$subdir_name.fasta" 12 -ic -load "$subdir_name/izlaz_ic"

echo "Izvrsavanje zavrseno"

done
