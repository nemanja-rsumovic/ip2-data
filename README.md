# ip2-data



## Getting started

Skup ulaznih podataka sadrzi podatke iz PAIDB baze (http://www.paidb.re.kr/about_paidb.php) o patogenim ostrvima bakterije **Escherichia coli**: http://www.paidb.re.kr/browse_genomes.php?m=g#Escherichia%20coli .

1. Analizirati odstupanja pojave sva 4 tipa ponavljajjucih sekvenci u ostrvima u odnosu na ostatak koda koristeci program StatRepeats iz paketa
http://bioinfo.matf.bg.ac.rs/home/downloads.wafl?cat=Software&project=RepeatsPlus

2. Sekvence Escherichia coli